import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  final _pageController = PageController(
    viewportFraction: 0.1,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text('Notifications'),
            backgroundColor: Colors.red,
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(Icons.notifications_rounded),
              ),
            ],
          ),
          body: Center(
            child: PageView.builder(
              physics: BouncingScrollPhysics(),
              pageSnapping: true,
              controller: _pageController,
              scrollDirection: Axis.vertical,
              itemBuilder: (context, index) => _builder(index),
            ),
          ),
        ),
      ),
    );
  }

  _builder(int index) {
    return NotificationsCard();
  }
}

class NotificationsCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        child: Card(
          semanticContainer: true,
          color: Colors.red[300],
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          elevation: 1,
          margin: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
        ),
      ),
    );
  }
}
